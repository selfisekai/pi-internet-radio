# pi-internet-radio

because that old radio on your fridge may play more noise than music sometimes.

requirements:
- [python3](https://www.python.org/) + [pip](https://pip.pypa.io/en/stable/)
- [mpv](https://mpv.io/) + libmpv

recommended:
- [git](https://git-scm.com/) (to download the code)

installing on raspbian/ubuntu:
```sh
sudo apt-get install mpv libmpv1 python3 python3-pip git
git clone https://codeberg.org/selfisekai/pi-internet-radio.git
cd pi-internet-radio
sudo pip3 install -r requirements.txt
cp stations.example.json stations.json
cp config.example.json config.json
# fill with your data with any editor
# specifying pin numbers: https://gpiozero.readthedocs.io/en/stable/recipes.html#pin-numbering
nano config.json
# change your station list as you wish
nano stations.json
```

license: [AGPL-3.0](LICENSE)
