#!/usr/bin/env python3

from mpv import MPV
from gpiozero import Button
from random import randint
from time import sleep
import json

stations = json.load(open('stations.json', 'r'))
config = json.load(open('config.json', 'r'))

class PiInternetRadio():
  current_stream = 0
  player = MPV(ytdl=True)
  power_switch = Button(pin=config['power_switch'], pull_up=config['pull_up'], bounce_time=config['bounce_time'])
  station_prev_switch = Button(pin=config['station_switch']['previous'], pull_up=config['pull_up'], bounce_time=config['bounce_time'])
  station_next_switch = Button(pin=config['station_switch']['next'], pull_up=config['pull_up'], bounce_time=config['bounce_time'])

  def play_current_station(self):
    self.player.play(list(stations.values())[self.current_stream])
  
  def switch_station_next(self):
    #print('switching station, current={}'.format(self.current_stream))
    self.current_stream += 1
    if self.current_stream >= len(stations):
      self.current_stream = 0
    self.play_current_station()
  
  def switch_station_prev(self):
    #print('switching station, current={}'.format(self.current_stream))
    self.current_stream -= 1
    if self.current_stream <= len(stations):
      self.current_stream = len(stations) - 1
    self.play_current_station()
  
  def switch_power(self):
    #print('switching power')
    if self.player:
      self.player.quit()
      self.player = None
    else:
      self.player = MPV(ytdl=True)
      self.play_current_station()
  
  def start(self):
    self.player.play(list(stations.values())[self.current_stream])
    self.power_switch.when_pressed = self.switch_power
    self.station_prev_switch.when_pressed = self.switch_station_prev
    self.station_next_switch.when_pressed = self.switch_station_next

pir = PiInternetRadio()
pir.start()

# some code needs to be running for it to actually play anything 🤷‍♀️
# pir.wait_for_playback() could also be used, but it would end at the end of playback,
# and since we play online streams, it would actually never happen
while True:
  sleep(9999)
